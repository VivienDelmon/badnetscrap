#! /usr/bin/python

import time
import csv
import badnet

from datetime import date

from selenium import webdriver
from selenium.webdriver.common.actions.action_builder import ActionBuilder
from selenium.webdriver.common.actions.mouse_button import MouseButton
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

periodStart=date(2023,9,1)
periodEnd=date.today()

csvFile = open(f"jeunes_depenses_{periodStart.isoformat()}_{periodEnd.isoformat()}.csv", "w")
fout = csv.writer(csvFile)

# enable headless mode in Selenium
options = Options()

def back(driver):
    action = ActionBuilder(driver)
    action.pointer_action.pointer_down(MouseButton.BACK)
    action.pointer_action.pointer_up(MouseButton.BACK)
    action.perform()

options.add_argument('--headless=new')

# initialize an instance of the chrome driver (browser)
driver = webdriver.Chrome(
    options=options,
    # other properties...
)

# needed for some scripts to run fine
driver.set_window_size(1920, 1200)

# visit your target site
driver.get('https://badnet.fr/connexion')

# login
driver.find_element(By.NAME, "login").send_keys(badnet.login)
driver.find_element(By.NAME, "pwd").send_keys(badnet.pwd)
connect = driver.find_element(By.CSS_SELECTOR, 'form button[type=submit]')#.click()
webdriver.ActionChains(driver).move_to_element(connect).click(connect).perform()

time.sleep(5)

driver.get('https://badnet.fr/portefeuille')

time.sleep(10)

driver.find_element(By.CSS_SELECTOR, '#navbar-second > ul > li:nth-child(3) > a').click()

time.sleep(10)

nbPlayers = len(driver.find_elements(By.CSS_SELECTOR, '#players > tbody > tr'))

players = []

total = 0

for pIdx in range(1, nbPlayers + 1):
    p = driver.find_element(By.CSS_SELECTOR, f'#players > tbody > tr:nth-child({pIdx})')
    payement = int(p.find_element(By.CSS_SELECTOR, 'td:nth-child(4)').text.split(" ")[0])
    if payement > 0:
        nameElt = p.find_element(By.CSS_SELECTOR, 'td:nth-child(2) > p > a')
        player = {}
        player["name"] = nameElt.text
        player["cost"] = 0
        nameElt.click()
        time.sleep(10)
        tournaments = driver.find_elements(By.CSS_SELECTOR, f'#paiements_club > tbody > tr')
        for t in tournaments:
            dateStr = t.find_element(By.CSS_SELECTOR, 'td:nth-child(1)').text
            origin = t.find_element(By.CSS_SELECTOR, 'td:nth-child(4)').text
            if dateStr == "Total" or origin != "En ligne" :
                continue
            dateL = dateStr.split("-")
            dateT = date(int(dateL[2]), int(dateL[1]), int(dateL[0]))
            if dateT < periodStart or dateT > periodEnd:
                continue
            price = int(t.find_element(By.CSS_SELECTOR, 'td:nth-child(3)').text.split(" ")[0])
            player["cost"] += price
            print (">>", dateT.isoformat(), price, t.find_element(By.CSS_SELECTOR, 'td:nth-child(2)').text)

        back(driver)
        time.sleep(10)
        players.append(player)
        if player["cost"] > 0:
            fout.writerow([player["name"], player["cost"]])
            print (player["name"], player["cost"])
            total += player["cost"]

fout.writerow(["Total", total])
print ("Total", total)

# release the resources allocated by Selenium and shut down the browser
driver.quit()
