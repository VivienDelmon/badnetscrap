#! /usr/bin/python

import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

options = webdriver.ChromeOptions()
options.add_argument('--headless=new')

# initialize an instance of the chrome driver (browser)
driver = webdriver.Chrome(
    options=options,
    # other properties...
)

# needed for some scripts to run fine
driver.set_window_size(1920, 1200)

driver.get('https://icbad.ffbad.org/instance/ACB38')

equipesElt = driver.find_elements(By.CSS_SELECTOR, "td.nom-equipe")

equipes = []

for eq in equipesElt:
    if "38-ACB" in eq.text:
        equipe = {}
        equipe["name"] = eq.text
        equipe["link"] = eq.find_element(By.CSS_SELECTOR, "a").get_attribute("href")
        equipes.append(equipe)
        print(eq.text, eq.find_element(By.CSS_SELECTOR, "a").get_attribute("href"))

teams_each_week = {}
for eq in equipes:
    print (eq["name"])
    driver.get(eq["link"])
    weeks = driver.find_elements(By.CSS_SELECTOR, "tr.clickable-row")
    for w in weeks:
        receiver = w.find_element(By.CSS_SELECTOR, 'td:nth-child(4)').text
        if receiver == eq["name"]:
            date = w.find_element(By.CSS_SELECTOR, 'td:nth-child(2)').text.split(" ")[1]
            if date not in teams_each_week:
                teams_each_week[date] = []
            teams_each_week[date].append(eq["name"].replace("Ass Crolloise Badminton (", "").replace(")", ""))
            print(">>" , w.find_element(By.CSS_SELECTOR, 'td:nth-child(2)').text.split(" ")[1])

for k in teams_each_week.keys():
    print (k , teams_each_week[k])

